package com.cgi.jira.service;

import com.cgi.jira.persistence.entities.Ticket;
import com.cgi.jira.persistence.repository.TicketRepository;
import com.cgi.jira.ws.TicketById;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TicketServiceImpl implements TicketService{

    private TicketRepository ticketRepository;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Override
    public TicketById getTicketById(Long id){
        Ticket ticket = ticketRepository.getTicketById(id);
        return mapTickeToTicketById(ticket);
    }

    private TicketById mapTickeToTicketById(Ticket ticket) {
        TicketById ticketById = new TicketById();
        ticketById.setIdTicket(ticket.getIdTicket());
        ticketById.setName(ticket.getName());
        ticketById.setIdTicketCreator(ticket.getIdPersonCreator());
        ticketById.setIdTicketAssigned(ticket.getIdPersonAssigned());
        ticketById.setCreationDate(ticket.getCreationDate());
        ticketById.setTicketCloseDate(ticket.getTicketCloseDate());
        return ticketById;
    }

    @Override
    public List<Ticket> getAllTickets(){
        return ticketRepository.getAllTickets();
    }

    @Override
    public int createTicket(Ticket ticket){
        return ticketRepository.createTicket(ticket);
    }
}
