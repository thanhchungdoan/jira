package com.cgi.jira.service;

import com.cgi.jira.persistence.entities.Ticket;
import com.cgi.jira.ws.TicketById;

import java.util.List;

public interface TicketService {

    TicketById getTicketById(Long id);

    List<Ticket> getAllTickets();

    int createTicket(Ticket ticket);
}
