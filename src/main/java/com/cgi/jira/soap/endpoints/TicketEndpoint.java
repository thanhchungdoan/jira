package com.cgi.jira.soap.endpoints;

import com.cgi.jira.service.TicketService;
import com.cgi.jira.ws.GetTicketByIdRequest;
import com.cgi.jira.ws.GetTicketByIdResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class TicketEndpoint {
    private static final String NAMESPACE_URI = "http://cgi.com/ws";

    private TicketService ticketService;

    @Autowired
    public TicketEndpoint(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetTicketByIdRequest")
    @ResponsePayload
    public GetTicketByIdResponse getTicketById(@RequestPayload GetTicketByIdRequest request) {
        GetTicketByIdResponse getTicketByIdResponse = new GetTicketByIdResponse();
        getTicketByIdResponse.setTicketById(ticketService.getTicketById(request.getId()));
        return getTicketByIdResponse;
    }

}
