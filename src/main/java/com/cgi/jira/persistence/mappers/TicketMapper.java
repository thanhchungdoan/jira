package com.cgi.jira.persistence.mappers;

import com.cgi.jira.persistence.entities.Ticket;
import org.springframework.jdbc.core.RowMapper;


import java.sql.ResultSet;
import java.sql.SQLException;

public class TicketMapper implements RowMapper<Ticket> {

    @Override
    public Ticket mapRow(ResultSet resultSet, int i) throws SQLException {
        Ticket ticket = new Ticket();
        ticket.setIdTicket(resultSet.getLong("id_ticket"));
        ticket.setName(resultSet.getString("name"));
        ticket.setIdPersonCreator(resultSet.getLong("id_person_creator"));
        ticket.setIdPersonAssigned(resultSet.getLong("id_person_assigned"));
        ticket.setTicketCloseDate(resultSet.getString("ticket_close_date"));
        ticket.setCreationDate(resultSet.getString("creation_date"));
        return ticket;
    }
}
