package com.cgi.jira.persistence.repository;

import com.cgi.jira.persistence.entities.Ticket;
import com.cgi.jira.persistence.mappers.TicketMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TicketRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public TicketRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public Ticket getTicketById(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE id_ticket=?",
                new Object[]{id}, new TicketMapper());
    }

    public List<Ticket> getAllTickets() {
        return jdbcTemplate.query("SELECT * FROM ticket", new TicketMapper());
    }


    public int createTicket(Ticket ticket) {
        return jdbcTemplate.update(
                "INSERT INTO ticket (id_ticket, name,id_person_creator, id_person_assigned, creation_date, ticket_close_date) " +
                        "values(?, ?, ?, ?, ?, ?)"
                , ticket.getIdTicket()
                , ticket.getName()
                , ticket.getIdPersonCreator()
                , ticket.getIdPersonAssigned()
                , ticket.getCreationDate()
                , ticket.getTicketCloseDate());
    }
}
