package com.cgi;


import com.cgi.jira.persistence.entities.Ticket;
import com.cgi.jira.service.TicketService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;


@SpringBootApplication
public class JiraApplication {

    public static void main(String[] args) {
        SpringApplication.run(JiraApplication.class, args);

//
//        AnnotationConfigApplicationContext annotationConfigApplicationContext =
//                new AnnotationConfigApplicationContext(JiraApplication.class);
//
//        // Bean = instance
//// nefunguje - chybi metadata (musime vytvorit pomoci @Repository
//// potom vytvorit instanci pomoci @Service a predat zavislost pomoci @Autowired)
//
//        TicketService ticketService = annotationConfigApplicationContext.getBean(TicketService.class);
////        Ticket ticket = ticketService.getTicketById(1L);
//        List<Ticket> ticket = ticketService.getAllTickets();
//        System.out.println(ticket);
//

    }
}
